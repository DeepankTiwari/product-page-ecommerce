




function allProduct() {

  let api = fetch("https://fakestoreapi.com/products")

  api
    .then((data) => {
      return data.json()
    })
    .then((data) => {

      let productClass = document.querySelector("article")
      productClass.innerHTML = ""

      data.map((element) => {

        let productClass = document.querySelector("article")
        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")

        productClass.append(product)

        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML = `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })
}






function singleProduct(id) {


  fetch(`https://fakestoreapi.com/products/${id}`)
    .then((res) => {
      return res.json()
    })
    .then((element) => {

      let singleProductClass = document.querySelector("article")
      singleProductClass.innerHTML = ""

      let singleProduct = document.createElement("div")
      let singleProductImg = document.createElement("div")
      let singleProductContent = document.createElement("div")

      singleProduct.append(singleProductImg, singleProductContent)
      singleProductClass.append(singleProduct)

      singleProduct.setAttribute("id", element.id)
      singleProduct.setAttribute("class", "singleProduct")
      singleProductImg.setAttribute("class", "singleProductImg")
      singleProductContent.setAttribute("class", "singleProductContent")

      singleProductImg.innerHTML = `<img class="image" src=${element.image} alt="product image">`

      singleProductContent.innerHTML = `<h2>$${element.price}</h2>`
      singleProductContent.innerHTML += `<p><strong>${element.title}</strong></p>`
      singleProductContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
      singleProductContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

      singleProduct.append(singleProductImg, singleProductContent)
    })
    .catch((error) => {
      console.error(error);
    });
}






function allElectronic() {

  let api = fetch("https://fakestoreapi.com/products/category/electronics")

  api
    .then((data) => {

      return data.json()
    })
    .then((data) => {
      let productClass = document.querySelector("article")
      productClass.innerHTML = ""
      data.forEach((element) => {

        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")
 
        productClass.append(product)

        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML = ""
        productContent.innerHTML = `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })


}



function allMenClothing() {

  let api = fetch("https://fakestoreapi.com/products/category/men's clothing")

  api
    .then((data) => {

      return data.json()
    })
    .then((data) => {

      let productClass = document.querySelector("article")
      productClass.innerHTML = ""
      data.forEach((element) => {

        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")

        let linker = document.createElement("div")
        
        productClass.append(product)


        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML = ""
        productContent.innerHTML = `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })


}






function allWomenClothing() {

  let api = fetch("https://fakestoreapi.com/products/category/women's clothing")

  api
    .then((data) => {

      return data.json()
    })
    .then((data) => {

      let productClass = document.querySelector("article")
      productClass.innerHTML = ""

      data.forEach((element) => {


        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")

       
        
        productClass.append(product)


        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML += `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })


}




function allJewelery() {

  let api = fetch("https://fakestoreapi.com/products/category/jewelery")

  api
    .then((data) => {

      return data.json()
    })
    .then((data) => {

      let productClass = document.querySelector("article")
      productClass.innerHTML = ""

      data.forEach((element) => {

        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")

        productClass.append(product)

        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML = `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })


}





function sortAllProductsbyPrice() {
  let api = fetch("https://fakestoreapi.com/products")
  api
    .then(res => {
      return res.json()
    })
    .then((data) => {

      data.sort((a_price, b_price) => {
        return a_price.price - b_price.price
      })

      let productClass = document.querySelector("article")
      productClass.innerHTML = ""

      data.forEach((element) => {

        let product = document.createElement("div")
        let productImg = document.createElement("div")
        let productContent = document.createElement("div")

        productClass.append(product)

        product.setAttribute("onclick", `singleProduct(${element.id})`)

        product.setAttribute("id", element.id)
        product.setAttribute("class", "product")
        productImg.setAttribute("class", "productimg")
        productContent.setAttribute("class", "productcontent")

        productImg.innerHTML = `<img src=${element.image}>`

        productContent.innerHTML = `<h2>$${element.price}</h2>`
        productContent.innerHTML += `<p><strong>${element.title}</strong></p>`
        productContent.innerHTML += `<p><strong>Ratings</strong> : ${element.rating.rate}</p>`
        productContent.innerHTML += `<p class="description">Description : ${element.description}</p>`

        product.append(productImg, productContent)
      })
    })
    .catch((err) => {
      console.log(err)
    })
}

function main() {
  allProduct()

}

main()